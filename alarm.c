/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <alarm.h>
#include <clock.h>
#include <led2.h>

uint8_t alarm_minutes = 0;
uint8_t alarm_hours = 0;
uint8_t alarm_set = 0;

void set_alarm_time(uint8_t hours, uint8_t minutes) {
  alarm_minutes = minutes;
  alarm_hours = hours;
  display_string[0] = 'D';
  display_string[1] = 'O';
  display_string[2] = 'N';
  display_string[3] = 'E';
  alarm_set = 1;
}

void trigger_alarm(uint8_t * shift_byte) {
  alarm_set = 0;
  led_on(shift_byte, 0); // bottom white
//  led_on(shift_byte, 1); // top white
//  led_on(shift_byte, 2); // green
  led_on(shift_byte, 3); // blue 445
//  led_on(shift_byte, 4); // blue 465
//  toggle_led(shift_byte, 0);
//  toggle_led(shi;
//  led2_on();
}

void alarm_check(uint8_t * shift_byte, uint8_t hours, uint8_t minutes) {
  if (alarm_set && hours == alarm_hours && minutes == alarm_minutes) {
    trigger_alarm(shift_byte);
  }
}


