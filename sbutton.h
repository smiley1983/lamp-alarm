/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SBUTTONH
#define SBUTTONH

#include <avr/io.h>
#include <stdint.h>

void clear_switch_states(void);
void clear_switch_state(uint8_t i);
void read_sbutton(void);
void pulse_bytes (uint8_t * shift_data);
void init_sbutton(void);
uint8_t sbutton_state(uint8_t i);
uint8_t up_pressed(void);
uint8_t down_pressed(void);
uint8_t left_pressed(void);
uint8_t right_pressed(void);

#endif
