/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <sbutton.h>
#include <shift.h>
#include <pin.h>
#include <macro.h>
#include <led2.h>

#define SBUTTON_BYTE_L 2
#define SBUTTON_BYTE_H 3

#define SBUTTON_BYTES 2

// probably need to divide all of this by about 16
#ifdef CRYSTAL
  #define DEBOUNCE_INITIAL 40
  #define DEBOUNCE_REPEAT 8
#else
  #define DEBOUNCE_INITIAL 3
  #define DEBOUNCE_REPEAT 1
#endif

struct sw {
  uint8_t state;
  uint16_t delay;
  uint8_t repeat;
};

uint8_t high_bit = 15; // ready to roll around to 0

struct sw control[SBUTTON_BYTES * BYTE];

void init_sbutton(void) {

  set_input(SBUTTON_PORTDIR, SBUTTON_PIN);
  output_low(SBUTTON_PORT, SBUTTON_PIN);

  for (uint8_t i = 0; i < (SBUTTON_BYTES * BYTE); i++) {
    control[i].state = 0;
    control[i].delay = 0;
    control[i].repeat = 0;
  }
}

void pulse_bytes (uint8_t * shift_data)
{
  high_bit++;
  uint8_t low_byte = 0;
  uint8_t high_byte = 0;
  if (high_bit >= 16) high_bit = 0;
  if (high_bit >= 8) {
    high_byte = 1 << (high_bit - 8);
  }
  else {
    low_byte = 1 << high_bit;
  };
  shift_data[SBUTTON_BYTE_L] = low_byte;
  shift_data[SBUTTON_BYTE_H] = high_byte;
}

void read_sbutton(void) {
  if (control[high_bit].delay == 0) {
    control[high_bit].state = !(bit_is_clear(SBUTTON_PINREG, SBUTTON_PIN));
    if (control[high_bit].state) {
      if (control[high_bit].repeat) {
        control[high_bit].delay = DEBOUNCE_REPEAT;
      }
      else {
        control[high_bit].delay = DEBOUNCE_INITIAL;
        control[high_bit].repeat = 1;
      }
    }
    else {
      control[high_bit].repeat = 0;
    }
  }
  else {
    control[high_bit].delay--;
  }
}

void clear_switch_state(uint8_t i) {
  control[i].state = 0;
}

void clear_switch_states(void) {
  for (uint8_t i=0; i<(SBUTTON_BYTES * BYTE); i++) {
    clear_switch_state(i);
  }
}

uint8_t sbutton_state(uint8_t i) {
  return control[i].state;
}

uint8_t up_pressed(void) {
  return sbutton_state(0);
}

uint8_t down_pressed(void) {
  return sbutton_state(6);
}

uint8_t left_pressed(void) {
  return sbutton_state(5);
}

uint8_t right_pressed(void) {
  return sbutton_state(4);
}


