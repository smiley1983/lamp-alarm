/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <led.h>
#include <macro.h>
#include <pin.h>

// FIXME add #defines for all ports as well as pins

uint8_t led1_state = 0;
uint8_t led2_state = 0;
uint8_t led3_state = 0;
uint8_t led4_state = 0;
uint8_t led5_state = 0;
uint8_t led6_state = 0;
uint8_t led7_state = 0;
uint8_t led8_state = 0;

void init_leds(void) {
  set_output(DDRC, PANEL1);
  set_output(DDRD, PANEL2);
  output_low(PANEL1_PORT, PANEL1);
  output_low(PANEL2_PORT, PANEL2);

  set_output(DDRD, LED1);
  set_output(DDRD, LED2);
  set_output(DDRB, LED3);
  set_output(DDRD, LED4);

  output_low(PORTD, LED1);
  output_low(PORTD, LED2);
  output_low(PORTB, LED3);
  output_low(PORTD, LED4);

  set_output(DDRC, LED5);
  set_output(DDRC, LED6);
  set_output(DDRC, LED7);
  set_output(DDRC, LED8);

  output_low(PORTC, LED5);
  output_low(PORTC, LED6);
  output_low(PORTC, LED7);
  output_low(PORTC, LED8);
}

void led1_on(void) {
  output_high(PORTD, LED1);
  led1_state = 1;
}

void led2_on(void) {
  output_high(PORTD, LED2);
  led2_state = 1;
}

void led3_on(void) {
  output_high(PORTB, LED3);
  led3_state = 1;
}

void led4_on(void) {
  output_high(PORTD, LED4);
  led4_state = 1;
}

void led1_off(void) {
  output_low(PORTD, LED1);
  led1_state = 0;
}

void led2_off(void) {
  output_low(PORTD, LED2);
  led2_state = 0;
}

void led3_off(void) {
  output_low(PORTB, LED3);
  led3_state = 0;
}

void led4_off(void) {
  output_low(PORTD, LED4);
  led4_state = 0;
}

void led5_on(void) {
  output_high(PORTC, LED5);
  led5_state = 1;
}

void led6_on(void) {
  output_high(PORTC, LED6);
  led6_state = 1;
}

void led7_on(void) {
  output_high(PORTC, LED7);
  led7_state = 1;
}

void led8_on(void) {
  output_high(PORTC, LED8);
  led8_state = 1;
}

void led5_off(void) {
  output_low(PORTC, LED5);
  led5_state = 0;
}

void led6_off(void) {
  output_low(PORTC, LED6);
  led6_state = 0;
}

void led7_off(void) {
  output_low(PORTC, LED7);
  led7_state = 0;
}

void led8_off(void) {
  output_low(PORTC, LED8);
  led8_state = 0;
}

void panel_state(void) {
  if (!(led1_state || led2_state || led3_state || led4_state)) {
    output_low(PANEL1_PORT, PANEL1);
  }
  else {
    output_high(PANEL1_PORT, PANEL1);
  };

  //if (!(led5_state || led6_state || led7_state || led8_state)) {
  //  output_low(PANEL2_PORT, PANEL2);
  //}
  //else {
    output_high(PANEL2_PORT, PANEL2);
  //}
}

void toggle_led(ledset_func set_on, ledset_func set_off, uint8_t *state) {
  (*state) = !(*state);
  if (*state) {
    set_on();
  }
  else {
    set_off();
  };
  panel_state();
}

void toggle_led1(void) {
  toggle_led(led1_on, led1_off, &led1_state);
}

void toggle_led2(void) {
  toggle_led(led2_on, led2_off, &led2_state);
}

void toggle_led3(void) {
  toggle_led(led3_on, led3_off, &led3_state);
}

void toggle_led4(void) {
  toggle_led(led4_on, led4_off, &led4_state);
}

void toggle_led5(void) {
  toggle_led(led5_on, led5_off, &led5_state);
}

void toggle_led6(void) {
  toggle_led(led6_on, led6_off, &led6_state);
}

void toggle_led7(void) {
  toggle_led(led7_on, led7_off, &led7_state);
}

void toggle_led8(void) {
  toggle_led(led8_on, led8_off, &led8_state);
}

