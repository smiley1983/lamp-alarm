/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SWITCHH
#define SWITCHH

#include <avr/io.h>
#include <stdint.h>

#define MENU_UP 4
#define MENU_DN 5
#define MENU_LF 6
#define MENU_RT 7
#define LED_RED660 4
#define LED_RED630 5
#define LED_BLUE445 6
#define LED_BLUE465 7
#define LED_WWA 8
#define LED_WWB 9
#define LED_GREEN 10
#define LED_AMBER 11
#define LED_SET1 12
#define LED_SET2 13
#define ALARM_SW 14
#define MENU_OK 15

void read_switch_states(void);

void clear_switch_state(uint8_t i);

uint8_t switch_state(uint8_t i);

void clear_switch_states(void);

void init_switch_states(void);

#endif

