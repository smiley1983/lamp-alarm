/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <switch.h>
#include <shift_in.h>

#ifdef CRYSTAL
  #define DEBOUNCE_INITIAL 600
  #define DEBOUNCE_REPEAT 120
#else
  #define DEBOUNCE_INITIAL 75
  #define DEBOUNCE_REPEAT 15
#endif

struct sw {
  uint8_t state;
  uint16_t debounce;
  uint8_t repeat;
};

struct sw control[SHIFTIN_BYTES * BYTE];

void init_switch_states(void) {
  for (uint8_t i = 0; i < (SHIFTIN_BYTES * BYTE); i++) {
    control[i].state = 0;
    control[i].debounce = 0;
    control[i].repeat = 0;
  }
}

void read_state(uint8_t i) {
  if (control[i].debounce == 0) {
    control[i].state = shift_bit_state(i);
    if (control[i].state) {
      if (control[i].repeat) {
        control[i].debounce = DEBOUNCE_REPEAT;
      }
      else {
        control[i].debounce = DEBOUNCE_INITIAL;
        control[i].repeat = 1;
      }
    }
    else {
      control[i].repeat = 0;
    }
  }
  else {
    control[i].debounce--;
  }
}

void read_switch_states(void) {
  shift_bytes_in();
  for (uint8_t i=0; i<(SHIFTIN_BYTES * BYTE); i++) {
    read_state(i);
  }
}

void clear_switch_state(uint8_t i) {
  control[i].state = 0;
}

void clear_switch_states(void) {
  for (uint8_t i=0; i<(SHIFTIN_BYTES * BYTE); i++) {
    clear_switch_state(i);
  }
}

uint8_t switch_state(uint8_t i) {
  return control[i].state;
}

