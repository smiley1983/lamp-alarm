/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SHIFTINH
#define SHIFTINH

#include <avr/io.h>
#include <stdint.h>
#include <macro.h>

#define BYTE 8
#define SHIFTIN_BYTES 1

void init_shift_in(void);
uint8_t shift_bit_state(uint8_t val);
void shift_bytes_in(void);
uint8_t shift_bit_in(void);
void latch_high (void);
void latch_low (void);
void clock_high (void);
void clock_low (void);

#endif

