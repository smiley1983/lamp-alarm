/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <menu.h>
#include <shift.h>
#include <clock.h>
//#include <button.h>
#include <set_time.h>
#include <alarm.h>
//#include <led.h>
//#include <switch.h>
#include <sbutton.h>
#include <led2.h>

#define MENU_ITEMS 6 // 7
#define MI_CLOCK 0
#define MI_SET 1
#define MI_AL 2
#define MI_CALI 3
#define MI_3CAL 4
#define MI_SAVE 5
#define MI_MENU 6

uint8_t menu_mode = 0;

void menu(uint8_t *shift_data) {
  if (menu_mode == MI_MENU) {
    display_string[0] = 'M';
    display_string[1] = 'E';
    display_string[2] = 'N';
    display_string[3] = 'U';
  }
  else if (menu_mode == MI_SET) {
    display_string[0] = 'S';
    display_string[1] = 'E';
    display_string[2] = 'T';
    display_string[3] = '?';
  }
  else if (menu_mode == MI_AL) {
    display_string[0] = 'A';
    display_string[1] = 'L';
    display_string[2] = '?';
    display_string[3] = '?';
  }
  else if (menu_mode == MI_CALI) {
    display_string[0] = 'C';
    display_string[1] = 'A';
    display_string[2] = 'L';
    display_string[3] = 'I';
  }
  else if (menu_mode == MI_3CAL) {
    display_string[0] = '3';
    display_string[1] = 'C';
    display_string[2] = 'A';
    display_string[3] = 'L';
  }
  else if (menu_mode == MI_SAVE) {
    display_string[0] = 'S';
    display_string[1] = 'A';
    display_string[2] = 'V';
    display_string[3] = 'E';
  }
  clock_display(shift_data);
}

/*
void menu_process_buttons(uint8_t *shift_data) {

  if ((menu_mode == 0) && left_pressed() && (led1_state || led2_state)) {
    clear_button_states();
    led1_off();
    led2_off();
  }
  else if ((menu_mode == 0) && up_pressed()) {
    clear_button_states();
    led1_on();
  }
  else if ((menu_mode == 0) && down_pressed()) {
    clear_button_states();
    led2_on();
  }

  if (menu_mode > 0 && menu_mode < MENU_ITEMS) {
    menu(shift_data);
  };

  if (menu_mode > 0 && menu_mode < MENU_ITEMS && left_pressed()) {
    menu_mode --;
    clear_button_states();
    if (menu_mode == 0) {
      update_display_string(clock_hours, clock_minutes);
    }
  }
  else if (menu_mode < MENU_ITEMS - 1 && right_pressed()) {
    menu_mode ++;
    clear_button_states();
  }
  else if (menu_mode == MI_SET && down_pressed()) {
    clear_button_states();
    menu_mode = MENU_ITEMS + MI_SET;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_SET) {
    set_time_process_buttons(&menu_mode, shift_data, set_clock_time);
  }
  else if (menu_mode == MI_AL && down_pressed()) {
    clear_button_states();
    menu_mode = MENU_ITEMS + MI_AL;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_AL) {
    set_time_process_buttons(&menu_mode, shift_data, set_alarm_time);
  }
}
*/

/*
void menu_process_buttons_test_lamp(uint8_t *shift_data) {

  if ((menu_mode == 0) && left_pressed()) {
    clear_button_states();
    toggle_led(shift_data, 0);
    //toggle_led1();
  }
  else if ((menu_mode == 0) && up_pressed()) {
    clear_button_states();
    toggle_led(shift_data, 1);
    //toggle_led2();
  }
  else if ((menu_mode == 0) && down_pressed()) {
    clear_button_states();
    toggle_led(shift_data, 2);
    //toggle_led3();
  }
  else if ((menu_mode == 0) && right_pressed()) {
    clear_button_states();
    toggle_led(shift_data, 3);
    //toggle_led4();
  }
}
*/

void menu_process_buttons_test_sbutton(uint8_t *shift_data) {

  pulse_bytes(shift_data);
  shift_bytes(shift_data);
  read_sbutton();
//  read_switch_states();

  if (menu_mode == 0) { // FIXME remove redundant copied of this check below

/*
    if ((menu_mode == 0) && sbutton_state(0)) {
      clear_switch_states();
      //toggle_led1();
//      display_string[0]='0';
    }
    else if ((menu_mode == 0) && sbutton_state(1)) {
      clear_switch_states();
      //toggle_led2();
//      display_string[0]='1';
    }
    else if ((menu_mode == 0) && sbutton_state(2)) {
      clear_switch_states();
      //toggle_led3();
//      display_string[0]='2';
    }
    else if ((menu_mode == 0) && sbutton_state(3)) {
      clear_switch_states();
      //toggle_led4();
//      display_string[0]='3';
    }
    else if ((menu_mode == 0) && sbutton_state(4)) {
      clear_switch_states();
//      toggle_led(shift_data, 7);
      //toggle_led5();
//      display_string[0]='4';
    }
    else if ((menu_mode == 0) && sbutton_state(5)) {
      clear_switch_states();
//      toggle_led(shift_data, 5);
      //toggle_led6();
//      display_string[0]='5';
    }
    else if ((menu_mode == 0) && sbutton_state(6)) {
      clear_switch_states();
//      toggle_led(shift_data, 6);
      //toggle_led7();
//      display_string[0]='6';
    }
    else if ((menu_mode == 0) && sbutton_state(7)) {
      clear_switch_states();
      //toggle_led(shift_data, 4);
      //toggle_led8();
//      display_string[0]='7';
    }
*/

    if ((menu_mode == 0) && sbutton_state(8)) {
      clear_switch_states();
      toggle_led(shift_data, 4);
//      display_string[0]='8';
    }
    else if ((menu_mode == 0) && sbutton_state(9)) {
      clear_switch_states();
      toggle_led(shift_data, 3);
//      display_string[0]='9';
    }
    else if ((menu_mode == 0) && sbutton_state(10)) {
      clear_switch_states();
      toggle_led(shift_data, 2);
//      display_string[0]='A';
    }
    else if ((menu_mode == 0) && sbutton_state(11)) {
      clear_switch_states();
      toggle_led(shift_data, 0);
//      display_string[0]='B';
    }
    else if ((menu_mode == 0) && sbutton_state(12)) {
      clear_switch_states();
      toggle_led(shift_data, 1);
//      display_string[0]='C';
    }
    else if ((menu_mode == 0) && sbutton_state(13)) {
      clear_switch_states();
      toggle_led(shift_data, 5);
//      display_string[0]='D';
    }
    else if ((menu_mode == 0) && sbutton_state(14)) {
      clear_switch_states();
      toggle_led(shift_data, 6);
//      display_string[0]='E';
    }
    else if ((menu_mode == 0) && sbutton_state(15)) {
      clear_switch_states();
      toggle_led(shift_data, 7);
//      display_string[0]='F';
    }
  }
  if (menu_mode > 0 && menu_mode < MENU_ITEMS) {
    menu(shift_data);
  };

  if (menu_mode > 0 && menu_mode < MENU_ITEMS && left_pressed()) { // left
    menu_mode --;
    clear_switch_states();
    if (menu_mode == 0) {
      update_display_string(clock_hours, clock_minutes);
    }
  }
  else if (menu_mode < MENU_ITEMS - 1 && right_pressed()) { // right
    menu_mode ++;
    clear_switch_states();
  }
  else if (menu_mode == MI_SET && down_pressed()) { // down
    clear_switch_states();
    menu_mode = MENU_ITEMS + MI_SET;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_SET) {
    set_time_process_buttons(&menu_mode, shift_data, set_clock_time);
  }
  else if (menu_mode == MI_AL && down_pressed()) { //down
    clear_switch_states();
    menu_mode = MENU_ITEMS + MI_AL;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_AL) {
    set_time_process_buttons(&menu_mode, shift_data, set_alarm_time);
  }
  else {
//    display_string[0]='-';
  }
}

/*
void menu_process_buttons_test_shiftin(uint8_t *shift_data) {

  read_switch_states();

  if ((menu_mode == 0) && switch_state(MENU_UP)) {
    clear_button_states();
    toggle_led1();
    shift_data[0]='1';
  }
  else if ((menu_mode == 0) && switch_state(MENU_DN)) {
    clear_button_states();
    toggle_led2();
    shift_data[1]='1';
  }
  else if ((menu_mode == 0) && switch_state(MENU_LF)) {
    clear_button_states();
    toggle_led3();
    shift_data[2]='1';
  }
  else if ((menu_mode == 0) && switch_state(MENU_RT)) {
    clear_button_states();
    toggle_led4();
    shift_data[3]='1';
  }
}
*/
