#include <led2.h>

#define LED_BYTE_L 4
#define LED_BYTE_H 5

#define LED_BYTES 2

#define PANEL1 7
#define PANEL2 6
#define HV_SW 5

void init_leds(uint8_t * shift_data) {
  shift_data[LED_BYTE_L] = 0;
  shift_data[LED_BYTE_H] = 0;
}

void update_power_byte(uint8_t * shift_data) {
  uint8_t panel1 = 0;
  uint8_t panel2 = 0;
  if (shift_data[LED_BYTE_H] & 0b1111) {
    panel1 = 1;
  };
  if (shift_data[LED_BYTE_H] & 0b11110000) {
    panel2 = 1;
  };
  if (panel1)
    shift_data[LED_BYTE_L] |= (0b00000000 & (panel1 << PANEL1));
  else
    shift_data[LED_BYTE_L] &= (0b11111111 & ~(1 << PANEL1));
  if (panel2)
    shift_data[LED_BYTE_L] |= (0b00000000 & (panel2 << PANEL2));
  else
    shift_data[LED_BYTE_L] &= (0b11111111 & ~(1 << PANEL2));
  if (panel1 || panel2) {
    shift_data[LED_BYTE_L] |= (1 << HV_SW);
  }
  else {
    shift_data[LED_BYTE_L] &= ~(1 << HV_SW);
  }
}

void toggle_led(uint8_t * shift_data, uint8_t i) {
  uint8_t change = (1 << i);
  shift_data[LED_BYTE_H] ^= change;
  update_power_byte(shift_data);
}

void led_on(uint8_t * shift_data, uint8_t i) {
  uint8_t change = (1 << i);
  shift_data[LED_BYTE_H] |= change;
  update_power_byte(shift_data);
}

