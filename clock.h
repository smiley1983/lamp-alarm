/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef CLOCKH
#define CLOCKH


#include <avr/io.h>

#define NUM_DISPLAY_CHARS 4
#define CLOCK_BYTE_L 0
#define CLOCK_BYTE_H 1

void set_clock_time(uint8_t hours, uint8_t minutes);
char digit_of_int(uint8_t v);
void update_display_string(uint8_t hours, uint8_t minutes);
void update_clock(uint8_t * shift_byte);

void clock_display(uint8_t *shift_data);
//void clock_display(uint16_t *shift_data);
void clock_display_flash_minutes(uint8_t flash_state, uint8_t *shift_data);
void clock_display_flash_hours(uint8_t flash_state, uint8_t *shift_data);

char display_string[NUM_DISPLAY_CHARS];
uint8_t clock_minutes;
uint8_t clock_hours;

void display_four_digit_int(uint16_t v);

#endif
