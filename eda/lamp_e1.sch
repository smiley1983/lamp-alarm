EESchema Schematic File Version 2
LIBS:lamp-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:lamp-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA8A-P IC1
U 1 1 5724CE22
P 2250 4000
F 0 "IC1" H 1500 5300 50  0000 L BNN
F 1 "ATMEGA8A-P" H 2750 2550 50  0000 L BNN
F 2 "DIL28" H 2250 4000 50  0000 C CIN
F 3 "" H 2250 4000 50  0000 C CNN
	1    2250 4000
	1    0    0    -1  
$EndComp
$Comp
L 74HC595 U1
U 1 1 5724CEB0
P 5200 2450
F 0 "U1" H 5350 3050 50  0000 C CNN
F 1 "74HC595" H 5200 1850 50  0000 C CNN
F 2 "" H 5200 2450 50  0000 C CNN
F 3 "" H 5200 2450 50  0000 C CNN
	1    5200 2450
	1    0    0    -1  
$EndComp
$Comp
L 74HC595 U2
U 1 1 5724CF29
P 5200 3850
F 0 "U2" H 5350 4450 50  0000 C CNN
F 1 "74HC595" H 5200 3250 50  0000 C CNN
F 2 "" H 5200 3850 50  0000 C CNN
F 3 "" H 5200 3850 50  0000 C CNN
	1    5200 3850
	1    0    0    -1  
$EndComp
$Comp
L YY2841AH-33 Common_Anode_Clock_Display1
U 1 1 5724DF08
P 7050 2550
F 0 "Common_Anode_Clock_Display1" H 8050 3000 50  0000 C CNN
F 1 "YY2841AH-33" H 8050 2900 50  0000 C CNN
F 2 "Displays_7-Segment:Cx56-12" H 7300 2200 50  0000 C CNN
F 3 "" H 7300 2200 50  0000 C CNN
	1    7050 2550
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-lamp #PWR01
U 1 1 57261426
P 1050 3300
F 0 "#PWR01" H 1050 3050 50  0001 C CNN
F 1 "GND" H 1050 3150 50  0000 C CNN
F 2 "" H 1050 3300 50  0000 C CNN
F 3 "" H 1050 3300 50  0000 C CNN
	1    1050 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3300 4500 3300
Wire Wire Line
	4500 2800 4500 3600
Wire Wire Line
	3250 3200 4400 3200
Wire Wire Line
	4400 2500 4400 3900
Wire Wire Line
	4400 3900 4500 3900
Connection ~ 4400 3200
Connection ~ 4500 3300
Wire Wire Line
	4400 2500 4500 2500
Wire Wire Line
	4500 2800 4300 2800
Wire Wire Line
	4300 2800 4300 2200
Wire Wire Line
	4300 2200 4500 2200
Wire Wire Line
	5900 2900 5900 3100
Wire Wire Line
	5900 3100 4200 3100
Wire Wire Line
	4200 3100 4200 3400
Wire Wire Line
	4100 3400 4100 2000
Wire Wire Line
	4100 2000 4500 2000
Wire Wire Line
	4200 3400 4500 3400
Wire Wire Line
	4100 3400 3250 3400
Wire Wire Line
	5900 2000 7350 2000
Wire Wire Line
	7350 2000 7350 2200
Wire Wire Line
	5900 2100 6600 2100
Wire Wire Line
	6600 2100 6600 3600
Wire Wire Line
	6600 3600 7750 3600
Wire Wire Line
	5900 2200 6500 2200
Wire Wire Line
	6500 2200 6500 3700
Wire Wire Line
	6500 3700 7850 3700
Wire Wire Line
	7850 3700 7850 3600
Wire Wire Line
	5900 2300 7700 2300
Wire Wire Line
	7700 2300 7700 2200
Wire Wire Line
	7700 2200 7850 2200
Wire Wire Line
	5900 2400 6700 2400
Wire Wire Line
	6700 2400 6700 2100
Wire Wire Line
	6700 2100 8350 2100
Wire Wire Line
	8350 2100 8350 2200
Wire Wire Line
	5900 2500 6400 2500
Wire Wire Line
	6400 2500 6400 3800
Wire Wire Line
	6400 3800 7950 3800
Wire Wire Line
	7950 3800 7950 3600
Wire Wire Line
	5900 2600 6300 2600
Wire Wire Line
	6300 2600 6300 1900
Wire Wire Line
	6300 1900 8850 1900
Wire Wire Line
	8850 1900 8850 2200
Wire Wire Line
	5900 2700 6300 2700
Wire Wire Line
	6300 2700 6300 3900
Wire Wire Line
	6300 3900 8050 3900
Wire Wire Line
	8050 3900 8050 3600
Wire Wire Line
	5900 3400 6200 3400
Wire Wire Line
	6200 3400 6200 4000
Wire Wire Line
	6200 4000 8150 4000
Wire Wire Line
	8150 4000 8150 3600
Wire Wire Line
	5900 3500 6100 3500
Wire Wire Line
	6100 3500 6100 4100
Wire Wire Line
	6100 4100 8250 4100
Wire Wire Line
	8250 4100 8250 3600
Wire Wire Line
	5900 3600 6550 3600
Wire Wire Line
	6550 3600 6550 4200
Wire Wire Line
	6550 4200 8350 4200
Wire Wire Line
	8350 4200 8350 3600
Wire Wire Line
	5900 3700 6350 3700
Wire Wire Line
	6350 3700 6350 4300
Wire Wire Line
	6350 4300 8450 4300
Wire Wire Line
	8450 4300 8450 3600
Wire Wire Line
	800  3300 1350 3300
Wire Wire Line
	1050 2300 4500 2300
Wire Wire Line
	4500 3700 3900 3700
Wire Wire Line
	3900 3700 3900 2300
Connection ~ 3900 2300
Connection ~ 1050 3100
Wire Wire Line
	1050 3100 1050 2300
Wire Wire Line
	800  3100 1350 3100
$Comp
L VCC #PWR02
U 1 1 57275A80
P 800 3100
F 0 "#PWR02" H 800 2950 50  0001 C CNN
F 1 "VCC" H 800 3250 50  0000 C CNN
F 2 "" H 800 3100 50  0000 C CNN
F 3 "" H 800 3100 50  0000 C CNN
	1    800  3100
	1    0    0    -1  
$EndComp
Connection ~ 800  3300
Connection ~ 1050 3300
Wire Wire Line
	800  3300 800  5550
Wire Wire Line
	800  5550 4900 5550
Wire Wire Line
	4900 5550 4900 4400
$EndSCHEMATC
