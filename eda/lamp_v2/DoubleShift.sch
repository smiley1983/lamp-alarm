EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:smiley-component
LIBS:lamp_v2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 19
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74HC595 U?
U 1 1 57C43C1D
P 3650 2600
AR Path="/57C43A7A/57C43C1D" Ref="U?"  Part="1" 
AR Path="/57C3DA02/57C4B328/57C43C1D" Ref="U2"  Part="1" 
AR Path="/57C3DA02/57C4E927/57C43C1D" Ref="U?"  Part="1" 
AR Path="/57C3DA1C/57C83F5A/57C43C1D" Ref="U4"  Part="1" 
AR Path="/57C3D9FF/57CB6F00/57C43C1D" Ref="U7"  Part="1" 
AR Path="/57CBD35E/57CBF1B7/57C43C1D" Ref="U2"  Part="1" 
F 0 "U7" H 3800 3200 50  0000 C CNN
F 1 "74HC595" H 3650 2000 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 3650 2600 50  0000 C CNN
F 3 "" H 3650 2600 50  0000 C CNN
	1    3650 2600
	1    0    0    -1  
$EndComp
$Comp
L 74HC595 U?
U 1 1 57C43C24
P 3650 4200
AR Path="/57C43A7A/57C43C24" Ref="U?"  Part="1" 
AR Path="/57C3DA02/57C4B328/57C43C24" Ref="U3"  Part="1" 
AR Path="/57C3DA02/57C4E927/57C43C24" Ref="U?"  Part="1" 
AR Path="/57C3DA1C/57C83F5A/57C43C24" Ref="U5"  Part="1" 
AR Path="/57C3D9FF/57CB6F00/57C43C24" Ref="U8"  Part="1" 
AR Path="/57CBD35E/57CBF1B7/57C43C24" Ref="U3"  Part="1" 
F 0 "U8" H 3800 4800 50  0000 C CNN
F 1 "74HC595" H 3650 3600 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 3650 4200 50  0000 C CNN
F 3 "" H 3650 4200 50  0000 C CNN
	1    3650 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2750 2600 2750
Wire Wire Line
	2600 2750 2600 5000
Wire Wire Line
	2600 4350 2950 4350
Wire Wire Line
	2700 4050 2950 4050
Wire Wire Line
	2700 1750 2700 4050
Wire Wire Line
	2700 2450 2950 2450
Wire Wire Line
	3350 3650 3350 3400
Wire Wire Line
	3350 3400 2700 3400
Connection ~ 2700 3400
Wire Wire Line
	2700 1750 3350 1750
Wire Wire Line
	3350 1750 3350 2050
Connection ~ 2700 2450
Wire Wire Line
	3350 3150 3350 3250
Wire Wire Line
	3350 3250 2600 3250
Connection ~ 2600 3250
Wire Wire Line
	3350 4750 3350 5000
Wire Wire Line
	3350 5000 2600 5000
Connection ~ 2600 4350
Wire Wire Line
	4350 3050 4350 3500
Wire Wire Line
	4350 3500 2950 3500
Wire Wire Line
	2950 3500 2950 3750
Text HLabel 2400 2150 0    60   Input ~ 0
SER_IN
Wire Wire Line
	2950 2150 2400 2150
Text HLabel 2400 2650 0    60   Input ~ 0
LATCH
Text HLabel 2400 2350 0    60   Input ~ 0
CLOCK
Wire Wire Line
	2400 2350 2950 2350
Wire Wire Line
	2400 2650 2950 2650
Text HLabel 4650 4650 2    60   Output ~ 0
SER_OUT
Wire Wire Line
	4350 4650 4650 4650
Text HLabel 4700 2150 2    60   Output ~ 0
AA
Text HLabel 4700 2250 2    60   Output ~ 0
AB
Text HLabel 4700 2350 2    60   Output ~ 0
AC
Text HLabel 4700 2450 2    60   Output ~ 0
AD
Text HLabel 4700 2550 2    60   Output ~ 0
AE
Text HLabel 4700 2650 2    60   Output ~ 0
AF
Text HLabel 4700 2750 2    60   Output ~ 0
AG
Text HLabel 4700 2850 2    60   Output ~ 0
AH
Text HLabel 4700 3750 2    60   Output ~ 0
BA
Text HLabel 4700 3850 2    60   Output ~ 0
BB
Text HLabel 4700 3950 2    60   Output ~ 0
BC
Text HLabel 4700 4050 2    60   Output ~ 0
BD
Text HLabel 4700 4150 2    60   Output ~ 0
BE
Text HLabel 4700 4250 2    60   Output ~ 0
BF
Text HLabel 4700 4350 2    60   Output ~ 0
BG
Text HLabel 4700 4450 2    60   Output ~ 0
BH
Wire Wire Line
	4350 2250 4700 2250
Wire Wire Line
	4350 2350 4700 2350
Wire Wire Line
	4350 2450 4700 2450
Wire Wire Line
	4350 2550 4700 2550
Wire Wire Line
	4350 2650 4700 2650
Wire Wire Line
	4350 2750 4700 2750
Wire Wire Line
	4350 2850 4700 2850
Wire Wire Line
	4350 3750 4700 3750
Wire Wire Line
	4350 3850 4700 3850
Wire Wire Line
	4350 3950 4700 3950
Wire Wire Line
	4350 4050 4700 4050
Wire Wire Line
	4350 4150 4700 4150
Wire Wire Line
	4350 4250 4700 4250
Wire Wire Line
	4350 4350 4700 4350
Wire Wire Line
	4350 4450 4700 4450
Wire Wire Line
	4350 2150 4700 2150
Wire Wire Line
	2550 2350 2550 3950
Wire Wire Line
	2550 3950 2950 3950
Connection ~ 2550 2350
Wire Wire Line
	2450 2650 2450 4250
Wire Wire Line
	2450 4250 2950 4250
Connection ~ 2450 2650
$EndSCHEMATC
