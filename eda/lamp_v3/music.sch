EESchema Schematic File Version 2
LIBS:rur
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:smiley-component
LIBS:lamp_v2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 20
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DFPLAYER_MINI U?
U 1 1 57F0BAA1
P 4750 3650
F 0 "U?" H 4750 3400 60  0000 C CNN
F 1 "DFPLAYER_MINI" H 4750 4100 60  0000 C CNN
F 2 "smiley-library:dfplayer-mini" H 4750 3650 60  0000 C CNN
F 3 "" H 4750 3650 60  0000 C CNN
	1    4750 3650
	1    0    0    -1  
$EndComp
$Comp
L R_Small R?
U 1 1 57F0C3D6
P 3700 3400
F 0 "R?" H 3730 3420 50  0000 L CNN
F 1 "R_Small" H 3730 3360 50  0000 L CNN
F 2 "" H 3700 3400 50  0000 C CNN
F 3 "" H 3700 3400 50  0000 C CNN
	1    3700 3400
	0    1    1    0   
$EndComp
$Comp
L R_Small R?
U 1 1 57F0C3FB
P 3700 3500
F 0 "R?" H 3730 3520 50  0000 L CNN
F 1 "R_Small" H 3730 3460 50  0000 L CNN
F 2 "" H 3700 3500 50  0000 C CNN
F 3 "" H 3700 3500 50  0000 C CNN
	1    3700 3500
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 3400 4150 3400
Wire Wire Line
	3800 3500 4150 3500
Wire Wire Line
	3600 3400 3100 3400
Wire Wire Line
	3600 3500 3100 3500
Text HLabel 3100 3400 0    60   Input ~ 0
RX
Text HLabel 3100 3500 0    60   Output ~ 0
TX
Wire Wire Line
	4150 3300 3100 3300
Wire Wire Line
	5350 3900 5700 3900
Wire Wire Line
	5700 2900 5700 3900
Wire Wire Line
	2100 2900 5700 2900
Wire Wire Line
	5350 3300 5350 3000
Wire Wire Line
	5350 3000 3100 3000
Wire Wire Line
	4150 3900 2450 3900
Wire Wire Line
	2450 3900 2450 2900
Connection ~ 2450 2900
Text HLabel 2100 2900 0    60   Input ~ 0
GND
Text HLabel 3100 3000 0    60   Output ~ 0
BUSY
Text HLabel 3100 3300 0    60   Input ~ 0
VCC
$EndSCHEMATC
