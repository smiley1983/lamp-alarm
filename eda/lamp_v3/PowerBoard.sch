EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:smiley-component
LIBS:lamp_v2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 20
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BARREL_JACK CON3
U 1 1 57C3F1EF
P 2800 2900
F 0 "CON3" H 2800 3150 50  0000 C CNN
F 1 "BARREL_JACK" H 2800 2700 50  0000 C CNN
F 2 "smiley-library:BARREL_JACK_nosilk" H 2800 2900 50  0000 C CNN
F 3 "" H 2800 2900 50  0000 C CNN
	1    2800 2900
	1    0    0    -1  
$EndComp
$Comp
L BARREL_JACK CON2
U 1 1 57C3F367
P 2750 3600
F 0 "CON2" H 2750 3850 50  0000 C CNN
F 1 "BARREL_JACK" H 2750 3400 50  0000 C CNN
F 2 "smiley-library:BARREL_JACK_nosilk" H 2750 3600 50  0000 C CNN
F 3 "" H 2750 3600 50  0000 C CNN
	1    2750 3600
	1    0    0    -1  
$EndComp
$Comp
L BARREL_JACK CON1
U 1 1 57C3F3BF
P 2700 4350
F 0 "CON1" H 2700 4600 50  0000 C CNN
F 1 "BARREL_JACK" H 2700 4150 50  0000 C CNN
F 2 "smiley-library:BARREL_JACK_nosilk" H 2700 4350 50  0000 C CNN
F 3 "" H 2700 4350 50  0000 C CNN
	1    2700 4350
	1    0    0    -1  
$EndComp
$Comp
L BARREL_JACK CON4
U 1 1 57C3F3F8
P 2900 5100
F 0 "CON4" H 2900 5350 50  0000 C CNN
F 1 "BARREL_JACK" H 2900 4900 50  0000 C CNN
F 2 "smiley-library:BARREL_JACK_nosilk" H 2900 5100 50  0000 C CNN
F 3 "" H 2900 5100 50  0000 C CNN
	1    2900 5100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
