EESchema Schematic File Version 2
LIBS:rur
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:smiley-component
LIBS:lamp_v2-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 19
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATMEGA8A-A IC1
U 1 1 57C3DB0B
P 2950 4200
F 0 "IC1" H 2200 5400 50  0000 L BNN
F 1 "ATMEGA8A-A" H 3450 2650 50  0000 L BNN
F 2 "Housings_QFP:LQFP-32_7x7mm_Pitch0.8mm" H 2950 4200 50  0000 C CIN
F 3 "" H 2950 4200 50  0000 C CNN
	1    2950 4200
	1    0    0    -1  
$EndComp
Text Label 3100 2150 0    60   ~ 0
VCC
Text Label 2000 5950 0    60   ~ 0
GND
$Comp
L R R10K_1
U 1 1 57C3DDDD
P 2350 2650
F 0 "R10K_1" V 2430 2650 50  0000 C CNN
F 1 "R" V 2350 2650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2280 2650 50  0000 C CNN
F 3 "" H 2350 2650 50  0000 C CNN
	1    2350 2650
	0    1    1    0   
$EndComp
$Comp
L Crystal_Small Y1
U 1 1 57C3E096
P 1350 4000
F 0 "Y1" H 1350 4100 50  0000 C CNN
F 1 "Crystal_Small" H 1350 3900 50  0000 C CNN
F 2 "Crystals:Q_49U3HMS" H 1350 4000 50  0000 C CNN
F 3 "" H 1350 4000 50  0000 C CNN
	1    1350 4000
	0    1    1    0   
$EndComp
$Comp
L C_Small C1
U 1 1 57C3E249
P 850 3900
F 0 "C1" H 860 3970 50  0000 L CNN
F 1 "C_Small" H 860 3820 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 850 3900 50  0000 C CNN
F 3 "" H 850 3900 50  0000 C CNN
	1    850  3900
	0    1    1    0   
$EndComp
$Comp
L C_Small C2
U 1 1 57C3E280
P 850 4100
F 0 "C2" H 860 4170 50  0000 L CNN
F 1 "C_Small" H 860 4020 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 850 4100 50  0000 C CNN
F 3 "" H 850 4100 50  0000 C CNN
	1    850  4100
	0    1    1    0   
$EndComp
Text Label 1350 3200 0    60   ~ 0
RST
Text Label 4250 3500 0    60   ~ 0
MOSI
Text Label 4250 3600 0    60   ~ 0
MISO
Text Label 4250 3700 0    60   ~ 0
SCK
Text Label 4250 3900 0    60   ~ 0
BTN
Text Label 4250 3400 0    60   ~ 0
LATCH
$Comp
L CONN_02X05 P2
U 1 1 57C63715
P 4950 6950
F 0 "P2" H 4950 7250 50  0000 C CNN
F 1 "CONN_02X05" H 4950 6650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x05" H 4950 6550 50  0000 C CNN
F 3 "" H 4950 5750 50  0000 C CNN
	1    4950 6950
	1    0    0    -1  
$EndComp
Text Label 5400 7150 0    60   ~ 0
GND
Text Label 4300 6750 0    60   ~ 0
MOSI
Text Label 4300 6950 0    60   ~ 0
RST
Text Label 4300 7050 0    60   ~ 0
SCK
Text Label 4300 7150 0    60   ~ 0
MISO
NoConn ~ 4700 6850
NoConn ~ 5200 7050
NoConn ~ 5200 6950
NoConn ~ 5200 6850
$Comp
L D_Schottky_Small D1
U 1 1 57C64620
P 5800 6750
F 0 "D1" H 5750 6830 50  0000 L CNN
F 1 "D_Schottky_Small" H 5520 6670 50  0000 L CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Vertical_KathodeUp" V 5800 6750 50  0000 C CNN
F 3 "" V 5800 6750 50  0000 C CNN
	1    5800 6750
	-1   0    0    1   
$EndComp
$Comp
L R R1
U 1 1 57C647B7
P 6250 6750
F 0 "R1" V 6330 6750 50  0000 C CNN
F 1 "R" V 6250 6750 50  0000 C CNN
F 2 "smiley-library:Resistor_Vertical_2.4" V 6180 6750 50  0000 C CNN
F 3 "" H 6250 6750 50  0000 C CNN
	1    6250 6750
	0    1    1    0   
$EndComp
$Comp
L ZENER D5.1
U 1 1 57C64EA2
P 6550 6950
F 0 "D5.1" H 6550 7050 50  0000 C CNN
F 1 "ZENER" H 6550 6850 50  0000 C CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Vertical_KathodeUp" H 6550 6950 50  0000 C CNN
F 3 "" H 6550 6950 50  0000 C CNN
	1    6550 6950
	0    1    1    0   
$EndComp
Text Label 6600 6750 0    60   ~ 0
VCC
Text Label 7850 3650 0    60   ~ 0
MOSI
Text Label 7850 3450 0    60   ~ 0
LATCH
Text Label 7850 3750 0    60   ~ 0
SCK
Text Label 7850 3550 0    60   ~ 0
VCC
Text Label 7850 3350 0    60   ~ 0
BTN
Text Label 7850 3850 0    60   ~ 0
GND
Text HLabel 8500 3750 2    60   Output ~ 0
SCK
Text HLabel 8500 3450 2    60   Output ~ 0
LATCH
Text HLabel 8500 3650 2    60   Output ~ 0
SER_OUT
Text HLabel 8500 3550 2    60   Output ~ 0
VCC
Text HLabel 8500 3350 2    60   Output ~ 0
BTN
Text HLabel 8500 3850 2    60   Output ~ 0
GND
$Comp
L D_Schottky_Small D2
U 1 1 57CB0DC3
P 6000 6350
F 0 "D2" H 5950 6430 50  0000 L CNN
F 1 "D_Schottky_Small" H 5720 6270 50  0000 L CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Vertical_KathodeUp" V 6000 6350 50  0000 C CNN
F 3 "" V 6000 6350 50  0000 C CNN
	1    6000 6350
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X02 P3
U 1 1 57CB0F14
P 6050 5700
F 0 "P3" H 6050 5850 50  0000 C CNN
F 1 "CONN_01X02" V 6150 5700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 6050 5700 50  0000 C CNN
F 3 "" H 6050 5700 50  0000 C CNN
	1    6050 5700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 2150 3000 2900
Wire Wire Line
	2900 2150 2900 2900
Connection ~ 3000 2150
Wire Wire Line
	1750 1650 1750 5950
Wire Wire Line
	1750 5950 3000 5950
Wire Wire Line
	2900 5950 2900 5800
Wire Wire Line
	3000 5950 3000 5800
Connection ~ 2900 5950
Wire Wire Line
	750  3600 2050 3600
Connection ~ 1750 3600
Wire Wire Line
	2050 3400 1850 3400
Wire Wire Line
	1850 3400 1850 2150
Connection ~ 2900 2150
Wire Wire Line
	1950 2650 2200 2650
Wire Wire Line
	2500 2650 2900 2650
Connection ~ 2900 2650
Wire Wire Line
	950  3900 2050 3900
Wire Wire Line
	950  4100 2050 4100
Connection ~ 1350 3900
Connection ~ 1350 4100
Wire Wire Line
	750  3600 750  4100
Connection ~ 750  3900
Wire Wire Line
	1200 3200 2050 3200
Wire Wire Line
	3950 3500 4800 3500
Wire Wire Line
	3950 3600 4800 3600
Wire Wire Line
	3950 3700 4800 3700
Wire Wire Line
	3950 3900 4800 3900
Wire Wire Line
	3950 3400 4800 3400
Wire Wire Line
	1950 2650 1950 3200
Connection ~ 1950 3200
Wire Wire Line
	5200 6750 5700 6750
Wire Wire Line
	5200 7150 6550 7150
Wire Wire Line
	4700 6750 4200 6750
Wire Wire Line
	4700 6950 4200 6950
Wire Wire Line
	4700 7050 4200 7050
Wire Wire Line
	4700 7150 4200 7150
Wire Wire Line
	5900 6750 6100 6750
Wire Wire Line
	6400 6750 6800 6750
Connection ~ 6550 6750
Wire Wire Line
	7600 3650 8500 3650
Wire Wire Line
	7600 3750 8500 3750
Wire Wire Line
	7600 3850 8500 3850
Wire Wire Line
	6200 3550 8500 3550
Wire Wire Line
	6150 3450 8500 3450
Wire Wire Line
	6100 3350 8500 3350
Wire Wire Line
	6000 5900 6000 6250
Wire Wire Line
	6000 6450 6000 6750
Connection ~ 6000 6750
Wire Wire Line
	6100 5900 6850 5900
Wire Wire Line
	6850 5900 6850 7400
Wire Wire Line
	6850 7400 6000 7400
Wire Wire Line
	6000 7400 6000 7150
Connection ~ 6000 7150
Wire Wire Line
	6000 6050 5350 6050
Connection ~ 6000 6050
Wire Wire Line
	6850 6050 7400 6050
Connection ~ 6850 6050
Text HLabel 5350 6050 0    60   Input ~ 0
5V_IN
Text HLabel 7400 6050 2    60   Input ~ 0
GND_IN
$Comp
L C_Small C3
U 1 1 57C93341
P 2150 1900
F 0 "C3" H 2160 1970 50  0000 L CNN
F 1 "C_Small" H 2160 1820 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2150 1900 50  0000 C CNN
F 3 "" H 2150 1900 50  0000 C CNN
	1    2150 1900
	1    0    0    -1  
$EndComp
$Comp
L C_Small C4
U 1 1 57C935AE
P 2500 1900
F 0 "C4" H 2510 1970 50  0000 L CNN
F 1 "C_Small" H 2510 1820 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2500 1900 50  0000 C CNN
F 3 "" H 2500 1900 50  0000 C CNN
	1    2500 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1800 2150 1650
Connection ~ 2150 1650
Wire Wire Line
	2500 1650 2500 1800
Connection ~ 2500 1650
Wire Wire Line
	2150 2000 2150 2150
Connection ~ 2150 2150
Wire Wire Line
	2500 2150 2500 2000
Connection ~ 2500 2150
Wire Wire Line
	1750 1650 2500 1650
Wire Wire Line
	1850 2150 3350 2150
Wire Wire Line
	2050 3500 1200 3500
Wire Wire Line
	3950 3200 4750 3200
Wire Wire Line
	3950 3300 4750 3300
Wire Wire Line
	3950 4000 4750 4000
Wire Wire Line
	3950 4100 4750 4100
Wire Wire Line
	3950 4200 4750 4200
Wire Wire Line
	3950 4300 4750 4300
Wire Wire Line
	3950 4400 4750 4400
Wire Wire Line
	3950 4500 4750 4500
Wire Wire Line
	3950 4600 4750 4600
Wire Wire Line
	3950 4800 4750 4800
Wire Wire Line
	3950 4900 4750 4900
Wire Wire Line
	3950 5000 4750 5000
Wire Wire Line
	3950 5100 4750 5100
Wire Wire Line
	3950 5200 4750 5200
Wire Wire Line
	3950 5300 4750 5300
Wire Wire Line
	3950 5400 4750 5400
Wire Wire Line
	3950 5500 4750 5500
$Comp
L CONN_01X08 P1
U 1 1 57F0E7BF
P 5750 1500
F 0 "P1" H 5750 1950 50  0000 C CNN
F 1 "CONN_01X08" V 5850 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 5750 1500 50  0000 C CNN
F 3 "" H 5750 1500 50  0000 C CNN
	1    5750 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1150 4900 1150
Wire Wire Line
	5550 1250 4900 1250
Wire Wire Line
	5550 1350 4900 1350
Wire Wire Line
	5550 1450 4900 1450
Wire Wire Line
	5550 1550 4900 1550
Wire Wire Line
	5550 1650 4900 1650
Wire Wire Line
	5550 1750 4900 1750
Wire Wire Line
	5550 1850 4900 1850
Text Label 4900 1150 0    60   ~ 0
PD3
Text Label 4900 1250 0    60   ~ 0
PD4
Text Label 4900 1350 0    60   ~ 0
GND
Text Label 4900 1450 0    60   ~ 0
VCC
Text Label 4900 1550 0    60   ~ 0
GND
Text Label 4900 1650 0    60   ~ 0
VCC
Text Label 4900 1750 0    60   ~ 0
PB6
Text Label 4900 1850 0    60   ~ 0
PB7
Text Label 1800 3900 0    60   ~ 0
PB6
Text Label 1800 4100 0    60   ~ 0
PB7
Text Label 4200 5100 0    60   ~ 0
PD3
Text Label 4200 5200 0    60   ~ 0
PD4
$Comp
L CONN_01X08 P5
U 1 1 57F0F422
P 7150 1500
F 0 "P5" H 7150 1950 50  0000 C CNN
F 1 "CONN_01X08" V 7250 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 7150 1500 50  0000 C CNN
F 3 "" H 7150 1500 50  0000 C CNN
	1    7150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1150 6200 1150
Wire Wire Line
	6950 1250 6200 1250
Wire Wire Line
	6950 1350 6200 1350
Wire Wire Line
	6950 1450 6200 1450
Wire Wire Line
	6950 1550 6200 1550
Wire Wire Line
	6950 1650 6200 1650
Wire Wire Line
	6950 1750 6200 1750
Wire Wire Line
	6950 1850 6200 1850
Text Label 4200 5300 0    60   ~ 0
PD5
Text Label 4200 5400 0    60   ~ 0
PD6
Text Label 4200 5500 0    60   ~ 0
PD7
Text Label 6200 1150 0    60   ~ 0
PD5
Text Label 6200 1250 0    60   ~ 0
PD6
Text Label 6200 1350 0    60   ~ 0
PD7
Text Label 6200 1450 0    60   ~ 0
PB0
Text Label 6200 1550 0    60   ~ 0
PB1
Text Label 4250 3200 0    60   ~ 0
PB0
Text Label 4250 3300 0    60   ~ 0
PB1
Text Label 6200 1650 0    60   ~ 0
LATCH
Text Label 6200 1750 0    60   ~ 0
MOSI
Text Label 6200 1850 0    60   ~ 0
MISO
$Comp
L CONN_01X08 P9
U 1 1 57F0F999
P 8550 1500
F 0 "P9" H 8550 1950 50  0000 C CNN
F 1 "CONN_01X08" V 8650 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 8550 1500 50  0000 C CNN
F 3 "" H 8550 1500 50  0000 C CNN
	1    8550 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 1150 7650 1150
Wire Wire Line
	8350 1250 7650 1250
Wire Wire Line
	8350 1350 7650 1350
Wire Wire Line
	8350 1450 7650 1450
Wire Wire Line
	8350 1550 7650 1550
Wire Wire Line
	8350 1650 7650 1650
Wire Wire Line
	8350 1750 7650 1750
Wire Wire Line
	8350 1850 7650 1850
Text Label 7650 1150 0    60   ~ 0
SCK
Text Label 7650 1250 0    60   ~ 0
VCC
Text Label 7650 1350 0    60   ~ 0
ADC6
Text Label 7650 1650 0    60   ~ 0
ADC7
Text Label 4250 4500 0    60   ~ 0
ADC6
Text Label 4250 4600 0    60   ~ 0
ADC7
Text Label 1300 3500 0    60   ~ 0
AREF
Text Label 7650 1450 0    60   ~ 0
AREF
Text Label 7650 1550 0    60   ~ 0
GND
Text Label 4250 4000 0    60   ~ 0
PC1
Text Label 4250 4100 0    60   ~ 0
PC2
Text Label 4250 4200 0    60   ~ 0
PC3
Text Label 4250 4300 0    60   ~ 0
PC4
Text Label 4250 4400 0    60   ~ 0
PC5
Text Label 7650 1750 0    60   ~ 0
BTN
Text Label 7650 1850 0    60   ~ 0
PC1
$Comp
L CONN_01X08 P10
U 1 1 57F104A7
P 10000 1500
F 0 "P10" H 10000 1950 50  0000 C CNN
F 1 "CONN_01X08" V 10100 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 10000 1500 50  0000 C CNN
F 3 "" H 10000 1500 50  0000 C CNN
	1    10000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 1150 9150 1150
Wire Wire Line
	9800 1250 9150 1250
Wire Wire Line
	9800 1350 9150 1350
Wire Wire Line
	9800 1450 9150 1450
Wire Wire Line
	9800 1550 9150 1550
Wire Wire Line
	9800 1650 9150 1650
Wire Wire Line
	9800 1750 9150 1750
Wire Wire Line
	9800 1850 9150 1850
Text Label 9150 1150 0    60   ~ 0
PC2
Text Label 9150 1250 0    60   ~ 0
PC3
Text Label 9150 1350 0    60   ~ 0
PC4
Text Label 9150 1450 0    60   ~ 0
PC5
NoConn ~ 9150 1550
Text Label 4200 4800 0    60   ~ 0
PD0
Text Label 4200 4900 0    60   ~ 0
PD1
Text Label 4200 5000 0    60   ~ 0
PD2
Text Label 9150 1650 0    60   ~ 0
PD0
Text Label 9150 1750 0    60   ~ 0
PD1
Text Label 9150 1850 0    60   ~ 0
PD2
$EndSCHEMATC
