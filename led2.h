#ifndef LED2H
#define LED2H

#include <avr/io.h>
#include <stdint.h>

void init_leds(uint8_t * shift_data);
void toggle_led(uint8_t * shift_data, uint8_t i);
void led_on(uint8_t * shift_data, uint8_t i);
void update_power_byte(uint8_t * shift_data);

#endif

