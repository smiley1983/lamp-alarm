/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <shift_in.h>
#include <pin.h>
#include <led2.h>

uint8_t input[SHIFTIN_BYTES];

void clock_high (void) {
  output_high (SHIFTIN_CLOCKPORT, SHIFTIN_CLOCKPIN);
}

void clock_low (void) {
  output_low (SHIFTIN_CLOCKPORT, SHIFTIN_CLOCKPIN);
}

void latch_high (void) {
  output_high (SHIFTIN_LATCHPORT, SHIFTIN_LATCHPIN);
}

void latch_low (void) {
  output_low (SHIFTIN_LATCHPORT, SHIFTIN_LATCHPIN);
}

void clock_pulse(void) {
  clock_low();
  for (uint16_t j = 0; j < 8; j++) {
    asm("nop;");
  };
  clock_high();
}

// shift_bit_in expects that latch has already been set 
uint8_t shift_bit_in(void) {
  clock_pulse();
  if (bit_is_clear (SHIFTIN_DATAPORT, SHIFTIN_DATAPIN)) {
    //toggle_led1();
    return 1;
  }
  else {
    //toggle_led1();
    return 0;
  }
}

// shift_byte_in expects that latch has already been set low
uint8_t shift_byte_in(void) {
  uint8_t result = 0;
  for (uint8_t i = 0; i < BYTE; i++) {
    result &= (shift_bit_in() << i);
  };
  return result;
}

void shift_bytes_in(void) {
  latch_high();
  for (uint16_t j = 0; j < 80; j++) {
    asm("nop;");
  };
  latch_low();
  for (uint8_t i=0; i < SHIFTIN_BYTES; i++) {
    input[i] = shift_byte_in();
  };
}

uint8_t shift_bit_state(uint8_t val) {
  uint8_t byte_val = val / 8;
  uint8_t bit_val = val % 8;
  return (input[byte_val] & (1 << bit_val));
}

void init_shift_in(void) {

  // Set pin modes. Data is input, latch and clock are outputs.
  // FIXME wrong wrong wrong. This should be DDRx, not SHIFTIN_xPORT
  set_input(SHIFTIN_DATAPORT, SHIFTIN_DATAPIN);
  set_output(SHIFTIN_LATCHPORT, SHIFTIN_LATCHPIN);
  set_output(SHIFTIN_CLOCKPORT, SHIFTIN_CLOCKPIN);

  // set pin directions
  output_low (SHIFTIN_DATAPORT, SHIFTIN_DATAPIN);
  latch_high();
  clock_high();
  
}

