/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SHIFTH
#define SHIFTH

#define BYTE 8
#define SHIFT_BYTES 6

#include <avr/io.h>
#include <stdint.h>
#include <macro.h>

struct byte_pair {
  uint8_t bh;
  uint8_t bl;
};

struct byte_pair bytes_of_uint16_t (uint16_t v);

void data_on (void); 
void data_off (void);

void shift_on (void);
void shift_off (void);

void store_on (void);
void store_off (void);

uint16_t get_char_bits (char c);
uint16_t get_shift_digit (uint8_t i);
void shift_output (uint16_t bits);
void shift_bytes (uint8_t *bytes);
void init_shift(void);

#endif
