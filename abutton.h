/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef BUTTONH
#define BUTTONH

#define NOTHING 0
#define B01 1
#define B02 2
#define B03 3
#define B04 4
#define B05 5
#define B06 6
#define B07 7
#define B08 8
#define B09 9
#define B10 10
#define B11 11
#define B12 12

#define MIN_PRESS 128
#define B01_VAL 256
#define B02_VAL 290
#define B03_VAL 330
#define B04_VAL 350
#define B05_VAL 380
#define B06_VAL 410
#define B07_VAL 440
#define B08_VAL 470
#define B09_VAL 500
#define B10_VAL 520
#define B11_VAL 550
#define B12_VAL 580

#ifdef CRYSTAL
  #define DEBOUNCE_TICKS 600
  #define DEBOUNCE_SETTLE 48
#else
  #define DEBOUNCE_TICKS 75
  #define DEBOUNCE_SETTLE 6
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

struct abutton {
  uint16_t reading;
  uint8_t pressed;
  uint8_t value_ready;
  uint8_t value;
  uint16_t debounce;
  uint8_t repeat;
};

void init_abutton(struct abutton b);

uint8_t read_abutton(struct abutton b);

uint8_t read_test_btn(void);

#endif

