/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <lamp.h>
//#include <button.h>
//#include <abutton.h>
#include <shift.h>
#include <macro.h>
#include <common.h>
#include <clock.h>
#include <menu.h>
//#include <led.h>
//#include <shift_in.h>
//#include <switch.h>
//#include <slowshift.h>
#include <sbutton.h>
#include <led2.h>

uint8_t shift_data[SHIFT_BYTES];

// Called upon Timer1 overflow if TOIE1 is set
ISR(TIMER1_COMPA_vect) // see pages 65, 136 and 140 of datasheet. TOV1 is set at TOP, triggering interrupt if TOIE1 is set in the TIMSK register
{ 
  uint8_t local_old_sreg = SREG; 
  cli();
  update_clock(shift_data);
//  clear_buttons();
  SREG = local_old_sreg;
  sei();
} 

void setup_timer(void) {
  TCCR1B = _BV(WGM12); // CTC mode
  #ifdef CRYSTAL
    TCCR1B |= _BV(CS10) | _BV(CS12); // timer at Fcpu / 1024
  #else
    TCCR1B |= _BV(CS10) | _BV(CS11); // timer at Fcpu / 64
  #endif
  OCR1A = CLOCK_TIMER_OVERFLOW; // Overflow after 1 second

  #ifdef ATMEGA328P
    TIMSK1 |= _BV(OCIE1A); // set output compare A match interrupt enable bit
  #else // assume atmega8a
    TIMSK |= _BV(OCIE1A); // set output compare A match interrupt enable bit
  #endif

  sei(); // were interrupts disabled ?
}

int main(void) {

//  uint8_t display_letter = 0;
  //uint16_t shift_data = 0;

  setup_timer();

  init_shift();

//  init_shift_in();

  init_sbutton();

  //init_buttons();

  init_leds(shift_data);
//  update_power_switch(shift_data);

//  init_switch_states();

  while (1) {
    //read_buttons();
//    slow_shift_byte_in();
    //menu_process_buttons(&shift_data);
//    menu_process_buttons_test_lamp(shift_data);
    //menu_process_buttons_test_shiftin(&shift_data);
    menu_process_buttons_test_sbutton(shift_data);
    if (menu_mode == 0) {
      clock_display(shift_data);
    }
  }
};

/*
int main(void) {

//  uint8_t display_letter = 0;
  uint16_t shift_data = 0;

  setup_timer();

  init_shift();

  init_shift_in();

  init_buttons();

  init_leds();

  while (1) {
    read_buttons();
    menu_process_buttons_test_lamp(&shift_data);
    if (menu_mode == 0) {
      clock_display(&shift_data);
    }
  }
}
*/

