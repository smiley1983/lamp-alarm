/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <slowshift.h>
#include <shift_in.h>
#include <busywait.h>
#include <pin.h>

void slow_clock_pulse(void) {
  clock_high();
  busywait(10, 10000);
  clock_low();
  busywait(10, 1000);
}

// shift_bit_in expects that latch has already been set 
uint8_t slow_shift_bit_in(void) {
  slow_clock_pulse();
  if (bit_is_clear (SHIFTIN_DATAPORT, SHIFTIN_DATAPIN)) {
    return 1;
  }
  else {
    return 0;
  }
}

uint8_t slow_shift_byte_in(void) {
  uint8_t result = 0;
  latch_high();
  busywait(100, 1000);
  latch_low();
  for (uint8_t i = 0; i < BYTE; i++) {
    result &= (slow_shift_bit_in() << i);
  };
  busywait(100, 1000);
  return result;
}




