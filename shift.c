/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//#include <stdlib.h>
#include <shift.h>
#include <pin.h>

void data_on (void) { output_high (PORTB, SHIFT_OUT); }
void data_off (void) { output_low (PORTB, SHIFT_OUT); }

void shift_on (void) { output_high (PORTB, SHIFT_CLK); }
void shift_off (void) { output_low (PORTB, SHIFT_CLK); }

void store_on (void) { output_high (PORTB, STORE_CLK); }
void store_off (void) { output_low (PORTB, STORE_CLK); }
//#define OLDBOARD

#ifdef OLDBOARD
#define DIGIT0_BIT 15
#define DIGIT1_BIT 12
#define DIGIT2_BIT 11
#define DIGIT3_BIT 9
#define DIGIT0 (1 << 15)
#define DIGIT1 (1 << 12)
#define DIGIT2 (1 << 11)
#define DIGIT3 (1 << 9)
#define SEGTOP (1 << 14)
#define SEGTOPLEFT (1 << 13)
#define SEGTOPRIGHT (1 << 10)
#define SEGMIDDLE (1 << 8)
#define SEGBOTTOMRIGHT (1 << 7)
#define SEGDOT (1 << 6)
#define SEGBOTTOM (1 << 5)
#define SEGBOTTOMLEFT (1 << 4)
#else
#define DIGIT0_BIT 19 - 15
#define DIGIT1_BIT 19 - 12
#define DIGIT2_BIT 19 - 11
#define DIGIT3_BIT 19 - 9
#define DIGIT0 (1 << (19 - 15))
#define DIGIT1 (1 << (19 - 12))
#define SEGTOP (1 << (19 - 14))
#define SEGTOPLEFT (1 << (19 - 13))
#define DIGIT2 (1 << (19 - 11))
#define DIGIT3 (1 << (19 - 9))
#define SEGTOPRIGHT (1 << (19 - 10))
#define SEGMIDDLE (1 << (19 - 8))
#define SEGBOTTOMRIGHT (1 << (19 - 7))
#define SEGDOT (1 << (19 - 6))
#define SEGBOTTOM (1 << (19 - 5))
#define SEGBOTTOMLEFT (1 << (19 - 4))

#endif
//#define DIGITS (1 << 15) | (1 << 12) | (1 << 11) | (1 << 9)
#define DIGITS DIGIT0 | DIGIT1 | DIGIT2 | DIGIT3

#define LETTER_QUERY DIGITS | SEGTOP | SEGTOPRIGHT | SEGMIDDLE | SEGBOTTOMLEFT

#define LETTER_0 DIGITS | SEGTOP | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOM | SEGBOTTOMLEFT | SEGBOTTOMRIGHT

#define LETTER_1 DIGITS | SEGTOPRIGHT | SEGBOTTOMRIGHT

#define LETTER_2 DIGITS | SEGTOP | SEGTOPRIGHT | SEGMIDDLE | SEGBOTTOM | SEGBOTTOMLEFT

#define LETTER_3 DIGITS | SEGTOP | SEGMIDDLE | SEGBOTTOM | SEGTOPRIGHT | SEGBOTTOMRIGHT

#define LETTER_4 DIGITS | SEGTOPRIGHT | SEGTOPLEFT | SEGMIDDLE | SEGBOTTOMRIGHT

#define LETTER_5 DIGITS | SEGTOP | SEGMIDDLE | SEGBOTTOM | SEGTOPLEFT | SEGBOTTOMRIGHT

#define LETTER_6 DIGITS | SEGTOP | SEGMIDDLE | SEGBOTTOM | SEGBOTTOMLEFT | SEGBOTTOMRIGHT | SEGTOPLEFT

#define LETTER_7 DIGITS | SEGTOP | SEGTOPRIGHT | SEGBOTTOMRIGHT

#define LETTER_8 DIGITS | SEGTOP | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOM | SEGBOTTOMLEFT | SEGBOTTOMRIGHT | SEGMIDDLE

#define LETTER_9 DIGITS | SEGTOP | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOM | SEGBOTTOMRIGHT | SEGMIDDLE

#define LETTER_A DIGITS | SEGTOP | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOMLEFT | SEGBOTTOMRIGHT | SEGMIDDLE

#define LETTER_C DIGITS | SEGTOP | SEGTOPLEFT | SEGBOTTOMLEFT | SEGBOTTOM

#define LETTER_D DIGITS | SEGTOPRIGHT | SEGBOTTOMRIGHT | SEGBOTTOM | SEGBOTTOMLEFT | SEGMIDDLE

#define LETTER_E DIGITS | SEGTOP | SEGMIDDLE | SEGBOTTOM | SEGTOPLEFT | SEGBOTTOMLEFT

#define LETTER_F DIGITS | SEGTOP | SEGMIDDLE | SEGTOPRIGHT | SEGBOTTOMRIGHT

#define LETTER_H DIGITS | SEGMIDDLE | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOMRIGHT | SEGBOTTOMLEFT

#define LETTER_N DIGITS | SEGMIDDLE | SEGBOTTOMRIGHT | SEGBOTTOMLEFT

#define LETTER_U DIGITS | SEGBOTTOM | SEGBOTTOMLEFT | SEGBOTTOMRIGHT | SEGTOPLEFT | SEGTOPRIGHT

#define LETTER_O DIGITS | SEGMIDDLE | SEGBOTTOM | SEGBOTTOMLEFT | SEGBOTTOMRIGHT

#define LETTER_I DIGITS | SEGBOTTOMLEFT | SEGTOPLEFT

#define LETTER_L DIGITS | SEGBOTTOMLEFT | SEGTOPLEFT | SEGBOTTOM

#define LETTER_J DIGITS | SEGTOPRIGHT | SEGBOTTOMRIGHT | SEGBOTTOM | SEGBOTTOMLEFT

#define LETTER_P DIGITS | SEGTOP | SEGMIDDLE | SEGTOPRIGHT | SEGTOPLEFT | SEGBOTTOMLEFT

#define LETTER_R DIGITS | SEGMIDDLE | SEGBOTTOMLEFT

#define LETTER_DASH DIGITS | SEGMIDDLE

#define LETTER_UNDERSCORE DIGITS | SEGBOTTOM

#define LETTER_SPACE DIGITS

struct byte_pair bytes_of_uint16_t (uint16_t v) {
  struct byte_pair b;
  b.bl = v & 0xff;
  b.bh = (v >> 8) & 0xff;
  return b;
}

uint16_t get_char_bits (char c) {
  switch (c) {
    case '?': // Empty
      //       d  dd d
      //     0b1001101000000000;
      // return 0b1001101000000000;
      return LETTER_SPACE;
    case '!':
      return DIGITS | SEGTOP;
    case '@':
      return DIGITS | SEGTOPLEFT;
    case '#':
      return DIGITS | SEGTOPRIGHT;
    case '$':
      return DIGITS | SEGMIDDLE;
    case '%':
      return DIGITS | SEGBOTTOM;
    case '^':
      return DIGITS | SEGBOTTOMRIGHT;
    case '&':
      return DIGITS | SEGBOTTOMLEFT;
    case '*':
      return DIGITS | SEGDOT;
    case '0':
    case 'Q':
      return LETTER_0;
      //return 0b1111111010110000;
    case '1':
      return LETTER_1;
      //return 0b1001111010000000;
    case '2':
    case 'Z':
      return LETTER_2;
      //return 0b1101111100110000;
    case '3':
    case 'M':
    case 'W':
      return LETTER_3;
      //return 0b1101111110100000;
    case 'Y':
    case '4':
      return LETTER_4;
      //return 0b1011111110000000;
    case '5':
    case 'S':
      return LETTER_5;
      //return 0b1111101110100000;
    case '6':
      return LETTER_6;
      //return 0b1111101110110000;
    case '7':
    case 'T':
      return LETTER_7;
      //return 0b1101111010000000;
    case '8':
    case 'B':
      return LETTER_8;
      //return 0b1111111110110000;
    case '9':
    case 'G':
      return LETTER_9;
      //return 0b1111111110100000;
    case 'A':
      return LETTER_A;
      //return 0b1111111110010000;
    case 'C':
      return LETTER_C;
      //return 0b1111101000110000;
    case 'D':
      return LETTER_D;
      //return 0b1001111110110000;
    case 'E':
      return LETTER_E;
      //return 0b1111101100110000;
    case 'F':
      return LETTER_F;
      //return 0b1111101100010000;
    case 'X':
    case 'K':
    case 'H':
      return LETTER_H;
      //return 0b1011111110010000;
    case 'N':
      return LETTER_N;
      //return 0b1001101110010000;
    case 'U':
    case 'V':
      return LETTER_U;
      //return 0b1011111010110000;
    case 'O':
      return LETTER_O;
      //return 0b1001101110110000;
    case 'I':
      return LETTER_I;
      //return 0b1011101000010000;
    case 'L':
      return LETTER_L;
      //return 0b1011101000110000;
    case 'J':
      return LETTER_J;
      //return 0b1001111010100000;
    case 'P':
      return LETTER_P;
      //return 0b1111111100010000;
    case 'R':
      return LETTER_R;
      //return 0b1001101100010000;
    case '-':
      return LETTER_DASH;
      //return 0b1001101100000000;
    default:
      return LETTER_QUERY;
      //return 0b1001101000000000;
  }
}

uint16_t get_shift_digit (uint8_t i) {
  switch (i) {
    case 0: return DIGIT0_BIT;
    case 1: return DIGIT1_BIT;
    case 2: return DIGIT2_BIT;
    default : return DIGIT3_BIT;
  }
}

void shift_output (uint16_t bits) {
  uint16_t i = 0;
  store_off();
  for (i = 0; i < SHIFT_BITS; i++) {
    data_off();
    shift_off();
    store_off();
    if (bits & (1 << i) ) {
      data_on();
    }
    shift_on();
  }
  store_on();
  shift_off();
  store_off();
}

void shift_byte (uint8_t byte) {
  for (uint8_t i = 0; i < BYTE; i++) {
    data_off();
    shift_off();
    store_off();
    if (byte & (1 << i)) {
      data_on();
    }
    shift_on();
  };
}

void shift_bytes (uint8_t *byte) {
  store_off();
  for (uint8_t index = 0; index < SHIFT_BYTES; index++) {
    shift_byte (byte[index]);
  }
  store_on();
  shift_off();
  store_off();
}

void init_shift(void) {
  // initialize the direction of the ports to be outputs
  // on the 3 pins connected to the first shift register
  set_output(DDRB, SHIFT_OUT);  
  set_output(DDRB, SHIFT_CLK);
  set_output(DDRB, STORE_CLK);

  output_high(PORTB, SHIFT_OUT);
  output_low(PORTB, SHIFT_CLK);
  output_high(PORTB, STORE_CLK);
  
}

