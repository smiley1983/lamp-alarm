/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <button.h>

#include <avr/io.h>
#include <stdint.h>
#include <macro.h>
#include <common.h>
#include <pin.h>

#ifdef CRYSTAL
  #define DEBOUNCE_TICKS 600
#else
  #define DEBOUNCE_TICKS 75
#endif

#define TIMER_LIMIT OCR1A

uint16_t btn_up_delay = 0;
uint16_t btn_dn_delay = 0;
uint16_t btn_lf_delay = 0;
uint16_t btn_rt_delay = 0;

uint8_t btn_up = 0;
uint8_t btn_dn = 0;
uint8_t btn_lf = 0;
uint8_t btn_rt = 0;

void init_buttons(void) {
  set_input(DDRB, BTN_UP);
  set_input(DDRB, BTN_LF);
  set_input(DDRD, BTN_DN);
  set_input(DDRD, BTN_RT);

  output_high(PORTB, BTN_UP);
  output_high(PORTB, BTN_LF);
  output_high(PORTD, BTN_DN);
  output_high(PORTD, BTN_RT);
}

uint8_t read_btn(uint8_t pinport, uint8_t pin, uint16_t *btn_delay, uint8_t *btn) {
  if (*btn_delay == 0) {
    (*btn) = (bit_is_clear (pinport, pin));
    if (*btn) {
      (*btn_delay) = DEBOUNCE_TICKS;
    }
  }
  else {
    (*btn_delay)--;
  }
  return (*btn);
}

void clear_button(uint16_t *btn_delay, uint8_t *btn) {
  if (*btn_delay == DEBOUNCE_TICKS) {
    (*btn_delay)--;
  }
  else {
    (*btn_delay) = 0;
    (*btn) = 0;
  }
}

void clear_buttons(void) {
  clear_button (&btn_up_delay, &btn_up);
  clear_button (&btn_up_delay, &btn_dn);
  clear_button (&btn_up_delay, &btn_lf);
  clear_button (&btn_up_delay, &btn_rt);
}

void clear_button_states(void) {
  btn_up = 0;
  btn_dn = 0;
  btn_lf = 0;
  btn_rt = 0;
}

void read_buttons(void) {
  read_btn(PINB, BTN_UP, &btn_up_delay, &btn_up);
  read_btn(PINB, BTN_LF, &btn_lf_delay, &btn_lf);
  read_btn(PIND, BTN_DN, &btn_dn_delay, &btn_dn);
  read_btn(PIND, BTN_RT, &btn_rt_delay, &btn_rt);
}

uint8_t up_pressed(void) {
  return btn_up;
}

uint8_t down_pressed(void) {
  return btn_dn;
}

uint8_t left_pressed(void) {
  return btn_lf;
}

uint8_t right_pressed(void) {
  return btn_rt;
}

/*
uint8_t up_pressed_consume(void) {
  uint8_t result = btn_up;
  btn_up = 0;
  return result;
}

uint8_t down_pressed_consume(void) {
  uint8_t result = btn_dn;
  btn_dn = 0;
  return result;
}

uint8_t left_pressed_consume(void) {
  uint8_t result = btn_lf;
  btn_lf = 0;
  return result;
}

uint8_t right_pressed_consume(void) {
  uint8_t result = btn_rt;
  btn_rt = 0;
  return result;
}
*/


