#ifndef PINH
#define PINH

/* Used pins:
 * PB0 PB1 PB2 PB3 PB4 PB5 PB6
 * PD3 PD4 PD5 PD6 PD7
 * PC0 PC1 PC2
 */ 

#define SHIFT_BITS 24
#define SHIFT_CLK PB5
#define STORE_CLK PB2
#define SHIFT_OUT PB3

#define LED1 PD5
#define LED2 PD6
#define LED3 PB2
#define LED4 PD3
#define PANEL1 PC5
#define PANEL1_PORT PORTC

#define LED5 PC1
#define LED6 PC2
#define LED7 PC3
#define LED8 PC4
#define PANEL2 PD2
#define PANEL2_PORT PORTD

#define HV_MOSFET PB0

#define BTN_UP PB1
#define BTN_LF PB0 //deprecated use of PB0 - now used for HV_MOSFET
#define BTN_DN PD7
#define BTN_RT PD4

/*
#define SHIFTIN_DATAPORT PORTC
#define SHIFTIN_DATAPIN PC0
#define SHIFTIN_CLOCKPORT PORTC
#define SHIFTIN_CLOCKPIN PC1
#define SHIFTIN_LATCHPORT PORTC
#define SHIFTIN_LATCHPIN PC2
*/

#define SBUTTON_PORTDIR DDRC
#define SBUTTON_PORT PORTC
#define SBUTTON_PIN PC0
#define SBUTTON_PINREG PINC

#endif
